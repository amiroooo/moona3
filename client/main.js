import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';

import './main.html';
import './groupList.html';

//////////game_state

let defaultRoundTime = 60 * 4;
let defaultReviewTime = 60 * 2;
let maxTableCanHold = 17;
var videoElement;


Session.set("intervalRound",0);

Meteor.startup(function() {
    //console.log('Meteor.startup');
});




var maxMinutes = defaultRoundTime;

clearInterval(self.groupHandle);
var twoMinutes = defaultReviewTime;


Template.game_state.onCreated(function() {
    Meteor.autorun(function() {

    });
});


Template.registerHelper('or',(a,b)=>{
    return a || b;
});

Template.game_state.helpers({
  backgroundPic: function(){
      return "mona3.png";
  }
});

Template.game_state.onRendered(function (){
  videoElement = document.getElementById("myvideo");
  var subs = Meteor.subscribe('gamestate');
  Meteor.autorun(function() {
    if (subs.ready()) {
      Session.set("currentSideDataIsDone",SideData.findOne({}).isDone);
      var gameState = GameState.findOne({});
      if (gameState){
        Session.set("currentRoundOrder",RoundsOrder.findOne({stage:gameState.gameStage}));
      }

    }
  });

});



// in order to make full screen
function toggleFullScreen() {
  if (!document.mozFullScreen && !document.webkitFullScreen) {
    if (videoElement.mozRequestFullScreen) {
      videoElement.mozRequestFullScreen();
    } else {
      videoElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else {
      document.webkitCancelFullScreen();
    }
  }
}

document.addEventListener("keydown", function(e) {
  if (e.keyCode == 13) {
    toggleFullScreen();
  }
}, false);


Template.registerHelper('equals', function (a, b) {
    return a === b;
});

Template.registerHelper('equals2', function (a, b, c) {
    return a === b || a === c;
});


//////////groupList


Template.groupList.onCreated(function() {
  groupCount = new ReactiveVar(0);
  skip = new ReactiveVar(0);
    self.groupHandle = Meteor.setInterval(function() {
    if (skip.get()+maxTableCanHold < groupCount.get()){
      skip.set(skip.get()+maxTableCanHold);
    }else{
      skip.set(0);
    }
  }, 10000);

});

// TODO need to show table points
Template.groupList.helpers({

  groups: function () {
    var startIndex=0;
    groupCount.set(Groups.find({}).count());
    items = Groups.find({}, {sort: {points: -1},skip:skip.get(), transform: function(item){
      startIndex = startIndex+1;
      item.rank = startIndex+skip.get();
      return item;
    }});
    itemF = items.fetch();
    itemF.splice(maxTableCanHold, itemF.length-maxTableCanHold);
    return itemF;
  },
  getGroupPoints: function(points, admin_extra){
    if (points == null){
      points = 0;
    }
    if (admin_extra == null){
      admin_extra = 0;
    }
    return Math.ceil(points+admin_extra);
  }
});

Template.groupList.onDestroyed(function () {
  Meteor.clearInterval(self.groupHandle);
});
