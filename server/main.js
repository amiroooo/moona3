import { Meteor } from 'meteor/meteor';
var bodyParser = Npm.require('body-parser');
Picker.middleware(bodyParser.urlencoded({ extended: false }));
Picker.middleware(bodyParser.json());

Meteor.startup(function () {
//Groups.remove({});
//Rounds.remove({});
//RoundParts.remove({});
    //if (!Groups.findOne()) {
        /*
        Groups.insert({groupName: "legendary team", points : 20});
        Groups.insert({groupName: "dragonborn", points : 32});
        Groups.insert({groupName: "quadcopter", points : 2});
        Groups.insert({groupName: "sucide squad", points : 67});
        Groups.insert({groupName: "power puffs", points : 23});
        */
    //}
    //Rounds.remove({});
    //if (!Rounds.findOne()) {
        //Rounds.insert({leftSide: "legendary team", rightSide : "dragonborn"});
        //Rounds.insert({leftSide: "dragonborn", rightSide : "sucide squad"});
        //Rounds.insert({leftSide: "quadcopter", rightSide : "amir"});
        //Rounds.insert({leftSide: "sucide squad", rightSide : "majd"});
        //Rounds.insert({leftSide: "power puffs", rightSide : "dragonborn"});

    //}
    //if (!Obstacles.findOne()) {
        //Obstacles.insert({name: "none", description:"none", owner:"none", maxPoints:0, tickPoints:0});
    //}

    //if (!FinalRanks.findOne()) {
        //FinalRanks.insert({first: "none", second:"none", third:"none"});
    //}

    //if (!RefreshPages.findOne()) {
        //RefreshPages.insert({auto:true});
    //}

    /*
    if (!RoundsOrder.findOne()) {
     RoundsOrder.remove({});
     Rounds.remove({ });
        var stage1 = RoundsOrder.findOne({stage: 1});

        RoundsOrder.update(stage1._id,{$set:{name:"Stage 1"}});

        var stage2 = RoundsOrder.findOne({stage: 2});
        RoundsOrder.update(stage2._id,{$set:{name:"Stage 2"}});

        var stage3 = RoundsOrder.findOne({stage: 3});
        RoundsOrder.update(stage3._id,{$set:{name:"Playoffs"}});

        RoundsOrder.insert({stage: 1, order:[]});
        RoundsOrder.insert({stage: 2, order:[]});
        RoundsOrder.insert({stage: 3, order:[]});
    }
    */

    //var gamestate = GameState.findOne();
    //console.log(JSON.stringify(gamestate));

    // just to log last page that was on
    //if (GameState.findOne()) {
        //GameState.remove({});
        //s = GameState.find({}).fetch();
        //console.log("init state: " + s[0].state);
    //}

    //if (Groups.findOne()) {
        //Groups.remove({});
        //s = Groups.find({}).fetch();
        //console.log("group 0: " + s[0].groupName);
    //}

    // in case no pages in db, will create new value with default value
    //if (!GameState.findOne()) {
    //    GameState.insert({state:"groupList", gameStage:1});
    //}

    //SideData.remove({});

    //t = SideData.findOne();
    //if (t) {
        //Groups.remove({});
        //console.log("sideData is available");

        //console.log("left old points: "+t.leftSide.oldPoints);
        //console.log("right old points: "+t.rightSide.oldPoints);
    //}
    //SideData.remove({});
    //if (!SideData.findOne()) {
    //    SideData.insert({leftSide:{groupName:"team_test1"},rightSide:{groupName:"team_test3"},roundTime:800000, action:0,isDone:0});
    //}

    if (!AndroidEvents.findOne()) {
        AndroidEvents.insert({action:"init",data:{}});
    }

});


Meteor.methods({
    addGroup: function (groupData) {
        var groupCount = Groups.find({groupName: groupData.groupName}).count();
        if (groupCount == 0) {
            var groupID = Groups.insert(groupData);
            return groupID;
        }
    },
    switchRefreshTables: function () {
        var t = RefreshPages.findOne({});
        var isAuto = true;
        if (t){
           if (t.auto) {
               isAuto = false;
           }else{
               isAuto = true;
           }
            RefreshPages.update(t._id,{$set : {auto:isAuto}});
        }
        return isAuto;
    },
    getServerTime: function(){
        return Date.now();
    }

});


function updateAndroid(data){
    var and = AndroidEvents.findOne();
    AndroidEvents.update(and._id,{$set:data});
}




//DONE
Picker.route( '/android/getgroups', function( params, request, response, next ) {
    t = Groups.find().fetch();

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify(t));
});



//Requires app to read it correctly
Picker.route( '/android/getrounds', function( params, request, response, next ) {
    var t = Rounds.find({}).fetch();
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;

    var responseObject = {data:t};
    response.end(JSON.stringify(responseObject));
});




//packet request has changed values need to update in app.
// roundId
// adds any data that is required
// phase1[successes]
// phase2{1,2,3,4,5,6,7,8,9[successes]}
// phase3[volt value]
// copter value
// catcher value
// innovability value
// add check if group already has 3 rounds refuse.
// request not done requires app first.
Picker.route( '/android/addround', function( params, request, response, next ) {

    var k = Rounds.find({group_id:  request.body[0], round_type : 0});
    var result_count = k.count();
    if (request.body[2]==0 && result_count >= 3){
      //var t = Rounds.find({}).fetch();
      response.end(JSON.stringify([]));
      return;
    }

    var kk = Rounds.find({group_id:  request.body[0], round_type : 1});
    var result_count2 = kk.count();
    if (request.body[2]==1 && result_count2 >= 1){
      //var t = Rounds.find({}).fetch();
      response.end(JSON.stringify([]));
      return;
    }

    var kkk = Rounds.find({group_id:  request.body[0], round_type : 2});
    var result_count = kkk.count();
    if (request.body[2]==2 && result_count >= 3){
      response.end(JSON.stringify([]));
      return;
    }


    var kkkk = Rounds.find({group_id:  request.body[0], round_type : 3});
    var result_count2 = kkkk.count();
    if (request.body[2] == 3 && result_count2 >= 1){
      response.end(JSON.stringify([]));
      return;
    }



    if (request.body[2] == 1 || request.body[2] == 3){
      result_count = 10;
    }

    addNewRound(request.body[0], request.body[1], request.body[2], result_count);

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});


function addNewRound(id, time, type, result_count){
  Rounds.insert({group_id:  id, round_time: time, round_type: type, progress:0, round_order : result_count+1},function(err,id){
    if (!err){
      initRoundData(id,type);
    }
  });
}





function initRoundData(id,round_type){
    switch (round_type){
      case 0:{ // field
        RoundParts.insert({round_id:  id, round_part_type: "phase1", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "phase2", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "phase3", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "copter", value: {data_type : "copter"}});
        RoundParts.insert({round_id:  id, round_part_type: "catcher", value: {data_type : "catcher"}});
        RoundParts.insert({round_id:  id, round_part_type: "innovation", value: {data_type : "innovation"}});
        break;
      }
      case 1:{ //comumnity
          RoundParts.insert({round_id:  id, round_part_type: "community_score", value: {data_type : "community_score"}});
          break;
      }
      case 2:{ // field
          RoundParts.insert({round_id:  id, round_part_type: "time", value: {data_type : "time"}});
          RoundParts.insert({round_id:  id, round_part_type: "story", value: {data_type : "story"}});
          RoundParts.insert({round_id:  id, round_part_type: "atmosphere", value: {data_type : "atmosphere"}});
          RoundParts.insert({round_id:  id, round_part_type: "puzzle_diversity", value: {data_type : "puzzle_diversity"}});
          RoundParts.insert({round_id:  id, round_part_type: "mechanism_diversity", value: {data_type : "mechanism_diversity"}});
          RoundParts.insert({round_id:  id, round_part_type: "clues", value: {data_type : "clues"}});
          RoundParts.insert({round_id:  id, round_part_type: "learning_connection", value: {data_type : "learning_connection"}});
          RoundParts.insert({round_id:  id, round_part_type: "effect", value: {data_type : "effect"}});
          RoundParts.insert({round_id:  id, round_part_type: "room_impression", value: {data_type : "room_impression"}});


          RoundParts.insert({round_id:  id, round_part_type: "excelled_puzzle", value: {data_type : "excelled_puzzle"}});
          RoundParts.insert({round_id:  id, round_part_type: "innovation", value: {data_type : "innovation"}});
          RoundParts.insert({round_id:  id, round_part_type: "team_work", value: {data_type : "team_work"}});

          break;
      }
      case 3:{ //comumnity
          RoundParts.insert({round_id:  id, round_part_type: "community_score", value: {data_type : "community_score"}});
          RoundParts.insert({round_id:  id, round_part_type: "escape_package", value: {data_type : "escape_package"}});
          break;
      }
    }
}


Picker.route('/android/deleteround/:_id', function(params, req, res, next) {
    var rp = RoundParts.find({round_id: params._id});

    rp.forEach((roundpart) => {
      RoundParts.remove(roundpart._id);
    });

    var round = Rounds.findOne({_id: params._id});

    if (round){
      var group_id = round.group_id;
      var deleted_round_order = round.round_order;
      var r = Rounds.find({'group_id':group_id, round_type: round.round_type, round_order: {$gt:deleted_round_order}});
      r.forEach((in_round) => {
        //console.log("round updated order to minus 1: " + in_round.round_order);
        var needed_round_or = in_round.round_order-1;
        Rounds.update(in_round._id,{$set: {round_order:needed_round_or}});
      });
    }


    t = Rounds.remove({_id:params._id});
    res.end("k");
});


Picker.route('/android/deletegroup/:_id', function(params, req, res, next) {
    var s = Groups.findOne({_id:params._id});
    var rounds = Rounds.find( { group_id : s._id }).fetch();

    for (var i=0; i<rounds.length ; i++){
        var rp = RoundParts.find({round_id: rounds[i]._id});
        rp.forEach((roundpart) => {
          RoundParts.remove(roundpart._id);
        });
        Rounds.remove({_id:rounds[i]._id});
    }

    t = Groups.remove({_id:params._id});
    res.end("");
});

// 0 round id
// 1 round type
// 2 round part type
// 3 full json data value

Picker.route( '/android/addnewroundpart', function( params, request, response, next ) {
    addRoundPartData(request.body[0],request.body[1],request.body[2],request.body[3]);
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});



// 0 roundpart id
// 1 updated json data
Picker.route( '/android/updateroundpart', function( params, request, response, next ) {
    //console.log("id: "+request.body[0]);
    //console.log("app status: "+request.body[1]);
    //console.log("data: "+request.body[3]);
    if ( is_change_round_values_allowed(request.body[2],request.body[1])){
        updateRoundPartDataByPartId(request.body[0],request.body[3]);
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});


Picker.route( '/android/resetgamepart', function( params, request, response, next ) {
    //console.log("id: "+request.body[0]); // roundpart id
    //console.log("app status: "+request.body[1]); // admin status
    //console.log("data: "+request.body[3]); // roundid
    if (request.body[1] == 0){
      var oldrp = RoundParts.findOne({_id:request.body[0]});
      if (oldrp){
        RoundParts.remove({_id:oldrp._id});
        RoundParts.insert({round_id:  oldrp.round_id, round_part_type: oldrp.round_part_type, value: {data_type : "unk"}});
      }
    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify([]));
});





function is_change_round_values_allowed(round_id,app_status){
  if (app_status == 0){
    return true;
  }
  var round = Rounds.findOne({_id:round_id});
  //console.log(JSON.stringify(round));
  if (round){
    if (round.progress == 0 && app_status < 3){
      return true;
    } else{
      return false;
    }
  }
}



Picker.route( '/android/updategrouppoints', function( params, request, response, next ) {
    //console.log("id" + request.body[0]);
    //console.log("points: "+request.body[1]);
    var g = Groups.findOne({_id: request.body[0]});
    if (g){
      Groups.update(request.body[0],{$set: {points:request.body[1]}});
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end("{}");
});



// full json data
function addRoundPartData(id,round_type,round_part_type,data){
  var t = Rounds.find({_id:  id}).fetch();
  delete data._id;
  if (t){
    var s = RoundParts.findOne({'round_part_type':  round_part_type});
    if (s){
      RoundParts.update(s._id, {$set: {value : data}});
    }
  }
}

// only updated json data
function updateRoundPartDataByPartId(id, data){
    var s = RoundParts.findOne({'_id':  id});
    if (s){
      var old_data = s.value;
      if (old_data == null){
        old_data = {};
      }
      var requiresRefresh = true;
      for (var key in data){
          if (data[key] == -1){
            delete data[key];
            delete old_data[key];
            requiresRefresh = false;
          }else{
            old_data[key] = data[key];
          }
      }
      old_data['requiresRefresh'] = requiresRefresh;
      //console.log( "part id: " + s._id);
      console.log( "new data: " + JSON.stringify(old_data));
      RoundParts.update(s._id, {$set: {value : old_data}});
    }
}


//TODO add rounditem


//only admin app should be able to send this request
Picker.route( '/android/addgroup', function( params, request, response, next ) {
    if (request.body[3] == 0) {
        Groups.insert({groupName:  request.body[0], description:request.body[1], number:request.body[2]});
    }
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        t = Groups.find().fetch();
        response.end(JSON.stringify(t));
});



//only admin app should be able to send this request
Picker.route( '/android/editgroup', function( params, request, response, next ) {
  if (request.body[3] == 0) {
    var group = Groups.findOne({_id: request.body[4]});
    if (group){
            var Jsonobj = {groupName:  request.body[0], description:request.body[1], number:request.body[2]};
            Groups.update(request.body[4], {$set: Jsonobj});
    }
  }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify({}));

});


function updateRound(round){
    Rounds.update(round._id,{$set:(round)});
}


Picker.route('/android/updateround', function(params, req, res, next) {
    Rounds.update(req.body[0],{$set: {leftSide:req.body[1],rightSide:req.body[2],timeLeft:Number(req.body[3])}});
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    var t = Rounds.find({}).fetch();
    res.end(JSON.stringify(t));

});


Picker.route('/android/updateroundref', function(params, req, res, next) {
    if (req.body[1] < 3 && req.body[0] != null){
      //console.log(req.body[0]);
      Rounds.update(req.body[0],{$set: {referee_name:req.body[2]}});
    }
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    res.end(JSON.stringify([req.body[2]]));
});

Picker.route('/android/updateroundpause', function(params, req, res, next) {
    if (req.body[1] < 3 && req.body[0] != null){
      //console.log(req.body[0]);
      if (req.body[2] == -1){
          Rounds.update(req.body[0],{$set: {pause_left:-1,progress:0}});
      }else{
          Rounds.update(req.body[0],{$set: {pause_left:req.body[2],progress:2}});
      }
    }
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    res.end(JSON.stringify([req.body[2]]));
});



Picker.route( '/android/show_winners', function( params, request, response, next ) {
    var state = Meteor.call('getGameState').state;
    Meteor.call('changeGameState', "winnerList");
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end("success");
});


Picker.route( '/android/addrawpoints', function( params, request, response, next ) {
    var groupId = request.body[0];
    var points = Number(request.body[1]);
    var g = Groups.findOne({_id: groupId});
    if (g){
      new_admin_extra = g.admin_extra;
      if (!isNaN(new_admin_extra)){
          new_admin_extra = new_admin_extra + points;
      }else{
          new_admin_extra = points;
      }

      Groups.update(g._id,{$set:{admin_extra:new_admin_extra}});

    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});


Picker.route( '/android/markrounddone', function( params, request, response, next ) {
    var k = Rounds.findOne({_id:  request.body[0]});
    if (k){
      Rounds.update(k._id,{$set: {progress:request.body[1]}});
      response.end(JSON.stringify({}));
    }
});

Picker.route( '/android/create_backup', function( params, request, response, next ) {
    var backupDBGroups = Groups.find({}).fetch();
    var backupDBRounds = Rounds.find({}).fetch();
    var backupDBRoundParts = RoundParts.find({}).fetch();

    for(var i = 0; i < backupDBGroups.length; i++) {
        backupDBGroups[i]['old_id'] = backupDBGroups[i]['_id'];
        delete backupDBGroups[i]['_id'];
        delete backupDBGroups[i]['points'];
        delete backupDBGroups[i]['admin_extra'];
    }
    var newBackupDBRounds = [];
    for(var i = 0; i < backupDBRounds.length; i++) {
        newBackupDBRounds[i] = {};
        newBackupDBRounds[i]['old_id'] = backupDBRounds[i]['_id'];
        newBackupDBRounds[i]['progress'] = 0;
        newBackupDBRounds[i]['group_id'] = backupDBRounds[i]['group_id'];
        newBackupDBRounds[i]['round_time'] = backupDBRounds[i]['round_time'];
        newBackupDBRounds[i]['round_type'] = backupDBRounds[i]['round_type'];
        newBackupDBRounds[i]['round_order'] = backupDBRounds[i]['round_order'];

    }

    for(var i = 0; i < backupDBRoundParts.length; i++) {
        delete backupDBRoundParts[i]['_id'];
        backupDBRoundParts[i]['progress'] = 0;
    }

    Backups.remove({});
    Backups.insert({groups :backupDBGroups, rounds: newBackupDBRounds, roundparts: backupDBRoundParts}, function (err, res) {
        var jsonArrayResponse = [];
        jsonArrayResponse.push("success");
        if (err){
            jsonArrayResponse.push("failed");
        }
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        response.end(JSON.stringify(jsonArrayResponse));
    });
});





var backup;

function resetGroup(theGroup){
    Groups.update(theGroup._id,{$set:{points:0}});
}


Picker.route( '/android/reset_group_points', function( params, request, response, next ) {
    //Meteor.call('changeGameState', "groupList");
    var groups = Groups.find({}).fetch();
    for (i=0;i<groups.length;i++){
        resetGroup(groups[i]);
    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});



var backupTimestamp = 0;
Picker.route( '/android/reset_season', function( params, request, response, next ) {
    var d = new Date();
    var n = d.getTime();
    if (n > backupTimestamp+30000){
      backupTimestamp = n;
      var backup = Backups.findOne({});
      if (backup){
        RoundParts.remove({});
        Rounds.remove({});
        Groups.remove({});
        var groups = backup.groups;
        var rounds = backup.rounds;
        var roundparts = backup.roundparts;

        for (i=0;i<groups.length;i++){
            Groups.insert({groupName:  groups[i].groupName, description:groups[i].description, number:groups[i].number, old_id: groups[i].old_id});
        }

        db_groups = Groups.find({}).fetch();
        for (i=0;i<db_groups.length;i++){
            var oid = db_groups[i]["old_id"];
            for (j=0;j<rounds.length;j++){
                if (rounds[j]["group_id"] == oid){
                    rounds[j]["group_id"] = db_groups[i]._id;

                }
            }
        }
        console.log("rounds length: "+ rounds.length);
        for (i=0;i<rounds.length;i++){
            console.log("i: " + i);
            addNewRound(rounds[i].group_id, rounds[i].round_time, rounds[i].round_type, rounds[i].round_order-1);
        }
      }
    }else{
      console.log("executed too fast so ignore");
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});
